Nexer R Dependencies
================
David Pinto (*Chief Analytics Officer*)

> Install correct version of required R packages

NexerStats Dependencies
-----------------------

Install `nexerstats` dependencies:

``` r
devtools::source_url("http://bitbucket.org/nexerlabs/nexer-r-dependencies/raw/master/install-nexerstats-dep.R")
```

Customized List of Dependencies
-------------------------------

To install packages from a customized list, first create a `txt` file like this:

| package  | version |  active|
|:---------|:--------|-------:|
| rmongodb | 1.8.0   |       0|
| MASS     | 7.3-44  |       0|
| mgcv     | 1.8-7   |       0|
| cluster  | 2.0.3   |       0|
| ggplot2  | 2.1.0   |       1|
| viridis  | 0.3.4   |       1|

Then, pass the list name as a `R` option, as follows:

``` r
options(pkg_list = "custom_list")
devtools::source_url("http://bitbucket.org/nexerlabs/nexer-r-dependencies/raw/master/install-nexerstats-dep.R")
```
